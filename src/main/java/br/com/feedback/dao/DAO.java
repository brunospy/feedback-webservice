package br.com.feedback.dao;

import java.util.List;

public interface DAO<T,K> {

	void persiste(T entity);

	void remove(K id);

	T update(T entity);

	T findById(K id);

	List<T> findAll();
	
}
