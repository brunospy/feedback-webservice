package br.com.feedback.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.feedback.dao.DAO;

@Repository
public abstract class DAOImpl<T, K> implements DAO<T, K> {

	@PersistenceContext
	protected EntityManager em;
	
	protected Class<T> entityClass;
	
	public DAOImpl(){
		this.entityClass = this.discoverPersistentClass();
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> discoverPersistentClass() {
      Type type = null;
      Class<?> clazz = getClass();

      while (clazz != null && !(type instanceof ParameterizedType)) {
    	  type = clazz != null ? clazz.getGenericSuperclass() : null;
    	  clazz = clazz.getSuperclass();
      }

      return (Class<T>) ((ParameterizedType)type).getActualTypeArguments()[0];
    }
	
	@Override
	@Transactional
	public void persiste(T entity) {
		em.persist(entity);
	}
	
	@Override
	@Transactional
	public void remove(K id) {
		em.remove(em.find(entityClass, id));
	}
	@Override
	@Transactional
	public T update(T entity) {
		return em.merge(entity);
	}
	
	@Override
	public T findById(K id) {
		return em.find(entityClass, id);
	}
	
	@Override
	public List<T> findAll() {
			CriteriaQuery<T> criteriaQuery = em.getCriteriaBuilder().createQuery(discoverPersistentClass());
			criteriaQuery.from(discoverPersistentClass());
			List<T> lista = em.createQuery(criteriaQuery).getResultList();
		return lista;
	}
	
}
