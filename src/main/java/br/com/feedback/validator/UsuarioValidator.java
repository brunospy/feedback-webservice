package br.com.feedback.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class UsuarioValidator {

	private final String patternEmail = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$";
	
	public Boolean isEmailValido(String email){
	   Pattern p = Pattern.compile(patternEmail); 
	   Matcher m = p.matcher(email); 
	   if (m.find()){
	      return false;
	   }
	   return true; 
	}

}
